const a = ['pensil-2', 'pensil-3', 'pensil-1', 'pensil-5', 'pensil-4'];
const n = a.length;

for (i = 0; i < n; i++) {

  for (j = i + 1; j < n; j++) {

    if (a[j] < a[i]) {

      [a[j], a[i]] = [a[i], a[j]];
    }
  }

}

console.log(a);
